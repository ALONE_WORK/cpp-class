# cpp class

#### Description
该仓库保存C++使用的一些库

#### C++ lib

1.  CJsonObject
2.  zlib
3.  MD5
4.  http

##### CJsonObject
> C++ load CJsonObject and how to use it.

1. development environment：visual studio2017 C++ console
2. CJsonObject library：download from network，url:https://gitee.com/Bwar/CJsonObject

###### project load CJsonObject
1. build a new folder cJsonObj in the project，and copy the xx.c and xx.h files to the new folder.
2. right click project-->property-->add-->new sizer-->cJsonObj
3. right click cJsonObj-->add-->existed items-->cJsonObject files.
4. add include：#include "cJsonObj/CJsonObject.hpp"

```
#include "cJsonObj/CJsonObject.hpp"
// CJsonObject add data
neb::CJsonObject root;
root.Add("password", "code");
root.Add("username", "juzi");
root.Add("ene_ratio", 1);
root.Add("Fs", 1000);
root.AddEmptySubArray("stations");
neb::CJsonObject arr1;
arr1.Add(1);
arr1.Add(1.1);
arr1.Add(1.2);
arr1.Add(1.3);
root["stations"].Add(arr1);
// json data to string
std::string s = root.ToString();
```
##### zlib
1. development environment：visual studio2017 C++ console
2. zlib library：download from network，url:https://www.zlib.net/

###### project load zlib
1. build a new folder zlib in the project,and copy the xx.c and xx.h files to the new folder.
2. right click project-->property-->add-->new sizer-->zlib.
3. right click zlib-->add-->existed items-->zlib files.
4. add include：#include "zlib/zlib.h"
5. right click project-->property-->C/C++-->preprocessor-->preprocessor definde-->add”_CRT_SECURE_NO_WARNINGS”和”_CRT_NONSTDC_NO_DEPRECATE”

```
#include "zlib/zlib.h"
unsigned char strSrc[] = "hello world,aaaa bbbb cccc";
unsigned char buf[1024] = { 0 };
unsigned char strDst[1024] = { 0 };
unsigned long srcLen = sizeof(strSrc);
unsigned long bufLen = sizeof(buf);
unsigned long dstLen = sizeof(strDst);
printf("Src string : %s \n Length: %ld\n", strSrc, srcLen);
/*compress*/
compress(buf, &bufLen, strSrc, srcLen);
printf("after compressed Length: %ld\n", bufLen);
printf("compressed string %s\n", buf);
/*uncompress*/
uncompress(strDst, &dstLen, buf, bufLen);
printf("after uncompressed length: %ld\n", dstLen);
printf("Uncompressed string %s\n", strDst);
```
##### MD5
1. development environment：visual studio2017 C++ console.
2. code by mysql reference network source, and upload to my website.

###### project load MD5
1. build a new folder zlib in the project,and copy the xx.c and xx.h files to the new folder.
2. right click project-->property-->add-->new sizer-->md5
3. right click md5-->add-->existed items-->MD5 files.
4. add include：#include "md5/MD5.h"

```
#include "md5/MD5.h"
MD5 md;
char * code = md.MDString(char*);
```
##### http
1. development environment：visual studio2017 C++ console
2. code by mysql reference network source, and upload to my website.

###### peoject load http
1. build a new folder zlib in the project,and copy the xx.c and xx.h files to the new folder.
2. right click project-->property-->add-->new sizer-->http
3. right click md5-->add-->existed items-->http files.
4. add include：#include "http/WindowsHttp.h"

```
#include "http/WindowsHttp.h"
WindowsHttp http;
std::string data((char*)buf);
std::string Header = "{\"Content-Encoding\": \"gzip\",\"X-Forwarded-For\" : \"8.8.8.8\"}";
std::string responeData = http.RequestJsonInfo("http://api.ankept.com/mslocation/v1.0", Hr_Post, Header, (char*)buf, bufLen);
std::cout << responeData << std::endl;
```
