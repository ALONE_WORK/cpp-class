# cpp class

#### 介绍
该仓库保存C++使用的一些库

#### C++库

1.  CJsonObject
2.  zlib
3.  MD5
4.  http

##### CJsonObject
> C++下如何加载CJsonObject以及使用，下面展开介绍

1. 开发环境：visual studio2017 C++控制台
2. CJsonObject库：直接从官网下载源码，网上很多，本文支架使用源码，地址：https://gitee.com/Bwar/CJsonObject

###### 项目引入CJsonObject
1. 在工程目录下新建cJsonObj文件夹，将下载的CJsonObject库中的xx.h、xx.c和xx.h文件全部复制到该目录下，CJsonObject库一个有4个文件
2. 右击项目-->属性-->添加-->新建筛选器-->cJsonObj
3. 右击cJsonObj-->添加-->现有项-->选中之前复制到工程目录下的所有文件
4. 添加头文件：#include "cJsonObj/CJsonObject.hpp"

```
#include "cJsonObj/CJsonObject.hpp"
// CJsonObject添加数据
neb::CJsonObject root;
root.Add("password", "code");
root.Add("username", "juzi");
root.Add("ene_ratio", 1);
root.Add("Fs", 1000);
root.AddEmptySubArray("stations");
neb::CJsonObject arr1;
arr1.Add(1);
arr1.Add(1.1);
arr1.Add(1.2);
arr1.Add(1.3);
root["stations"].Add(arr1);
// 将json数据转换为字符串，输出无格式
std::string s = root.ToString();
```
##### zlib
1. 开发环境：visual studio2017 C++控制台
2. zlib库：直接从官网下载源码，本文支架使用源码，地址：https://www.zlib.net/

###### 项目引入zlib
1. 在工程目录下新建zlib文件夹，将下载的zlib库中所有的xx.c和xx.h文件全部复制到该目录下
2. 右击项目-->属性-->添加-->新建筛选器-->zlib
3. 右击zlib-->添加-->现有项-->选中之前复制到工程目录下的所有文件
4. 添加头文件：#include "zlib/zlib.h"
5. 右击项目-->属性-->C/C++-->预处理器-->预处理器定义-->添加”_CRT_SECURE_NO_WARNINGS”和”_CRT_NONSTDC_NO_DEPRECATE”

```
#include "zlib/zlib.h"
unsigned char strSrc[] = "hello world,aaaa bbbb cccc";
unsigned char buf[1024] = { 0 };
unsigned char strDst[1024] = { 0 };
unsigned long srcLen = sizeof(strSrc);
unsigned long bufLen = sizeof(buf);
unsigned long dstLen = sizeof(strDst);
printf("Src string : %s \n Length: %ld\n", strSrc, srcLen);
/*压缩*/
compress(buf, &bufLen, strSrc, srcLen);
printf("after compressed Length: %ld\n", bufLen);
printf("compressed string %s\n", buf);
/*解压缩*/
uncompress(strDst, &dstLen, buf, bufLen);
printf("after uncompressed length: %ld\n", dstLen);
printf("Uncompressed string %s\n", strDst);
```
##### MD5
1. 开发环境：visual studio2017 C++控制台
2. 该接口代码参考网上代码自己编写的，可到本人上传的网站下载

###### 项目引入zlib
1. 在工程目录下新建MD5文件夹，将下载的MD5库中所有的MD5.cpp和MD5.h文件全部复制到该目录下
2. 右击项目-->属性-->添加-->新建筛选器-->md5
3. 右击zlib-->添加-->现有项-->选中之前复制到工程目录下的所有文件
4. 添加头文件：#include "md5/MD5.h"

```
#include "md5/MD5.h"
MD5 md;
char * code = md.MDString(char*);
```
##### http
1. 开发环境：visual studio2017 C++控制台
2. http接口代码参考网上代码自己编写的，可到本人上传的网站下载

###### 项目引入http
1. 在工程目录下新建http文件夹，将下载的http库中WindowsHttp.c和WindowsHttp.h文件复制到该目录下
2. 右击项目-->属性-->添加-->新建筛选器-->http
3. 右击zlib-->添加-->现有项-->选中之前复制到工程目录下的所有文件
4. 添加头文件：#include "http/WindowsHttp.h"

```
#include "http/WindowsHttp.h"
WindowsHttp http;
std::string data((char*)buf);
std::string Header = "{\"Content-Encoding\": \"gzip\",\"X-Forwarded-For\" : \"8.8.8.8\"}";
std::string responeData = http.RequestJsonInfo("http://api.ankept.com/mslocation/v1.0", Hr_Post, Header, (char*)buf, bufLen);
std::cout << responeData << std::endl;
```
